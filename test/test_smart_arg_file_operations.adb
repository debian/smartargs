with Text_IO;
with Smart_Args_File_Operations;
use Smart_Args_File_Operations;

procedure Test_Smart_Arg_File_Operations is
  

begin

  if File_Can_Be_Opened_Read_Only("bogus_name_that_does_not_exists.hopefully") then
    Text_IO.Put_Line("FAIL: Said non-existant file could be read");
  end if;
  
  if not File_Can_Be_Opened_Read_Only("test_smart_arg_file_operations.adb") then
    Text_IO.Put_Line("FAIL: Said file could not be read but we are pretty sure it exists");
  end if;

  if Is_A_Directory("bogus_name_that_does_not_exists.hopefully") then
    Text_IO.Put_Line("FAIL: Said a file that did not exist was a directory");
  end if;
  
  if Is_A_Directory("test_smart_arg_file_operations.adb") then
    Text_IO.Put_Line("FAIL: Said a regular file was a directory");
  end if;

  if not Is_A_Directory(".") then
    Text_IO.Put_Line("FAIL: Said current directory was not a directory");
  end if;
  
  if Is_A_Writeable_Directory("bogus_name_that_does_not_exists.hopefully") then
    Text_IO.Put_Line("FAIL: Said a file that did not exists was a writeable directory");
  end if;
  
  if not Is_A_Writeable_Directory(".") then
    Text_IO.Put_Line("FAIL: Said current directory was not writeable but it probably is.");
  end if;

  --
  -- It would be nice to create a directory and take away write priviledges and see what
  -- Is_Writeable_Directory_Says. Not sure how to do that under windows.
  --

end Test_Smart_Arg_File_Operations;

