with Smart_Arguments;
with Text_IO;
use Smart_Arguments;

procedure Test_Smart_Args is

  Help     : Smart_Arguments.Argument_Type;
  Strip    : Smart_Arguments.Argument_Type;
  Location : Smart_Arguments.Argument_Type;
  Simple_1 : Smart_Arguments.Argument_Type;
  Required_Pos_2, Required_Pos_1 : Smart_Arguments.Argument_Type;

  Bad_1, Bad_2, Bad_3 : Smart_Arguments.Argument_Type;

  Output_Width : Smart_Arguments.Argument_Type;

  procedure Show_Arg_Status (Arg : in Smart_Arguments.Argument_Type) is

  begin

    Text_IO.Put(Smart_Arguments.Argument_Image(Arg));
    if Smart_Arguments.Argument_Present(Arg) then
      Text_IO.Put_Line(" is present.");
    else
      Text_IO.Put_Line(" is not present.");
    end if;

  end Show_Arg_Status;


begin

  Create_Positional_Argument(Argument => Required_Pos_1,
    Position => 1,
    Required => true,
    Short_Description => "filename",
    Description => "A required positional argument");


  Set_Argument_Validity_As_Input_Filename(Argument => Required_Pos_1);

  Create_Positional_Argument(Argument => Required_Pos_2,
    Position => 2,
    Required => false,
    Short_Description => "optional",
    Description => "An optional positional argument");



  Create_Argument(Argument => Help,
    Short_Form => "-h",
    Long_Form => "--help",
    Required => false,
    Description => "Displays this help page");

  Create_Argument(Argument => Strip,
    Short_Form => "-s",
    Long_Form => "--strip",
    Required => false,
    Description => "Strips the symbols from the given binary");


  Create_Argument(Argument => Location,
    Short_Form => "-l",
    Long_Form => "--location",
    Required => false,
    Number_Required_Subargs => 3,
    Subarg_Description => "X Y Z",
    Description => "Specicies the X Y Z location on the grid from which the measurement will be made");


  Create_Argument(Argument => Output_Width,
    Short_Form => "-w",
    Long_Form => "--output_width",
    Required => true,
    Number_Required_Subargs => 1,
    Subarg_Description => "Integer",
    Description => "Specifies the output terminal column width that should be used when formatting the help");

  Set_Argument_Validity
    (Argument => Output_Width,
    Min => 10,
    Max => 180);


  --
  -- Nothing too interesting here. Just creating an argument with everything defaulted
  -- except short form to show the simplest case of argument generation.
  --
  Create_Argument(Argument => Simple_1,
    Short_Form => "-z");


  begin
    --
    -- Create an argument that "overlaps" and existing argument and verify
    -- the problem is detected.

    Create_Argument(Argument => Bad_1,
      Short_Form => "-w");
    Text_IO.Put_Line("**** FAIL FAIL FAIL. Failed to detect duplicate argument Bad_1");

  exception
    when Duplicate_Argument_Error =>
      null; -- Good. We wanted this to fail.
  end;

  begin
    --
    -- Create an argument that "overlaps" and existing argument and verify
    -- the problem is detected.

    Create_Argument(Argument => Bad_2,
      Short_Form => "--location");
    Text_IO.Put_Line("**** FAIL FAIL FAIL. Failed to detect duplicate argument Bad_2");

  exception
    when Duplicate_Argument_Error =>
      null; -- Good. We wanted this to fail.
  end;


  begin
    --
    -- Create an argument that "overlaps" and existing argument and verify
    -- the problem is detected.

    Create_Argument(Argument => Bad_3,
      Short_Form => "-V",
      Long_Form => "-s");
    Text_IO.Put_Line("**** FAIL FAIL FAIL. Failed to detect duplicate argument Bad_3");

  exception
    when Duplicate_Argument_Error =>
      null; -- Good. We wanted this to fail.
  end;

  Text_IO.Put_Line("Short form help");
  Text_IO.Put_Line("------------------------------------");

  Display_Help(Short_Form_Only => true);
  Text_IO.Put_Line("------------------------------------");

  Text_IO.New_line;
  Text_IO.Put_Line("Long form help");

  Text_IO.Put_Line("------------------------------------");

  if Smart_Arguments.Argument_Present(Output_Width) then

    Display_Help(Short_Form_Only => false,
      Format_For_Output_Width => Integer'value(Smart_Arguments.Get_Subargument(Output_Width, 1)),
      Pre_Argument_Command_Summary => "This is a test program that is only designed " &
      "to exercise the various capabilities of the smart_arguments package. " &
      "The only argument that has any real function is -w. The rest are just " &
      "indicative of some typical argument types.");

  else

    Display_Help(Short_Form_Only => false, Format_For_Output_Width => 70);


  end if;

  Show_Arg_Status(Location);
  Show_Arg_Status(Help);
  Show_Arg_Status(Strip);
  Show_Arg_Status(Output_Width);
  Show_Arg_Status(Simple_1);

  Show_Arg_Status(Required_Pos_1);
  Show_Arg_Status(Required_Pos_2);

  if All_Required_Arguments_Present then

    Text_IO.Put_Line("All required arguments were present on command line");

  else

    Text_IO.Put_Line("Some required arguments were not found");

  end if;


  if Arguments_Valid(Error_On_Suspicious_Arguments => true) then

    Text_IO.Put_Line("Arguments appear valid.");

  else

    Text_IO.Put_Line("Arguments do not appear valid");

  end if;

  Text_IO.Put_Line(Get_Command_Line_Error_Description);


end Test_Smart_args;
